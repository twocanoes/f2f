//
//  AppDelegate.m
//  F2F
//
//  Created by Timothy Perfitt on 5/4/21.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (strong) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {

    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.allowsOtherFileTypes=YES;
    openPanel.allowsMultipleSelection=YES;
    openPanel.message=@"Select a folder or group of folders";
    openPanel.canChooseDirectories=YES;
    NSModalResponse res=[openPanel runModal];

    if (res!=NSModalResponseOK) {
        [NSApp terminate:self];
    }
    NSArray *pathURLs=openPanel.URLs;

    NSFileManager *fm=[NSFileManager defaultManager];



    [pathURLs enumerateObjectsUsingBlock:^(NSURL *url, NSUInteger idx, BOOL * _Nonnull stop) {


        NSString *parentFolder=[url.path stringByDeletingLastPathComponent];

        NSString *name=[url.path.lastPathComponent stringByDeletingPathExtension];
        NSString *tempFolderName=[name stringByAppendingString:@"-com.twocanoes.f2f"];
        NSString *tempFolderPath=[parentFolder stringByAppendingPathComponent:tempFolderName];
        NSString *finalFolderPath=[parentFolder stringByAppendingPathComponent:name];

        NSString *destinationPath=[tempFolderPath stringByAppendingPathComponent:url.lastPathComponent];
        NSError *err;
        if([fm createDirectoryAtPath:tempFolderPath withIntermediateDirectories:NO attributes:nil error:&err]==NO){

            [[NSAlert alertWithError:err] runModal];
            return;
        }
        if([fm moveItemAtPath:url.path toPath:destinationPath error:&err]==NO){
            [[NSAlert alertWithError:err] runModal];
            return;
        }

        if([fm moveItemAtPath:tempFolderPath toPath:finalFolderPath error:&err]==NO){
            [[NSAlert alertWithError:err] runModal];
            return;


        }

    }];

    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Done";
    alert.informativeText=@"Completed";

    [alert addButtonWithTitle:@"OK"];
    [alert runModal];

    [NSApp terminate:self];

}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
