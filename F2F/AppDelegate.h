//
//  AppDelegate.h
//  F2F
//
//  Created by Timothy Perfitt on 5/4/21.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

